package com.demo.spring;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StudentRowMapper implements RowMapper<Student> {

	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {

		Student student = new Student();
		student.setSid(rs.getInt("id"));
		student.setName(rs.getString("name"));
		student.setAdd(rs.getString("addre"));

		return student;
	}

}
