package com.demo.spring;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

public interface DBOperation {
	
	public void setDataSource(DataSource ds);
	
	public int insert(Student student );
	
	public int updateDeatils(Student student );
	
	public int deleteDetails(int no );
	
	public int countStudents();
	
	public Map getStudentDeatils(int no);
	
	public List<Student> getStudentsDeatils();
}
