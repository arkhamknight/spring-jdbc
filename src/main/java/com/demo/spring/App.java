package com.demo.spring;

import org.springframework.context.ApplicationContext; 
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new FileSystemXmlApplicationContext(
				"src/main/java/com/demo/spring/cfgs/config.xml");

		DBOperation dbOperation = (DBOperation) context.getBean("studentJdbcTemplate");

		Student st = context.getBean("student", Student.class);
		
		System.out.println(dbOperation.insert(st));
	}
}
